use anyhow::{bail, Context};

#[derive(Clone, Debug, PartialEq)]
pub struct Since {
    pub date: chrono::NaiveDate,
}

impl std::str::FromStr for Since {
    type Err = anyhow::Error;

    fn from_str(value: &str) -> Result<Self, Self::Err> {

        let result = human_date_parser::from_human_time(value)
            .context(format!("Failed to parse '{value}' as a date specification"))?;

        let result = match result {
            human_date_parser::ParseResult::Date(date) => date,
            human_date_parser::ParseResult::DateTime(date_time) => date_time.date_naive(),
            human_date_parser::ParseResult::Time(_) => bail!("Specified `since` parameter was interpreted as a time. Only dates are supported."),
        };

        Ok(Since { date: result })
    }
}
impl From<chrono::NaiveDate> for Since {
    fn from(date: chrono::NaiveDate) -> Self {
        Self { date }
    }
}


#[cfg(test)]
mod tests {
    use std::str::FromStr;

    use super::*;

    #[test]
    fn should_parse_dates() -> anyhow::Result<()> {

        assert_eq!(
            Since::from_str("2020-12-31")?,
            date(2020, 12, 31)
        );

        assert_eq!(
            Since::from_str("2020-12-31 23:59")?,
            date(2020, 12, 31)
        );

        assert!(
            Since::from_str("23:59")
                .is_err()
        );

        Ok(())
    }

    fn date(year: u16, month: u8, day: u8) -> Since {
        Since::from(
            chrono::NaiveDate::from_ymd_opt(year.into(), month.into(), day.into()).unwrap()
        )
    }
}
