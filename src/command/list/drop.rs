use std::{ffi::OsString, path::PathBuf};

use super::since::Since;

///Requires that `files` is sorted alphabetically.
pub fn drop_from_sorted_files_before(since: Since, files: &mut Vec<PathBuf>) {
    let since = OsString::from(since.date.to_string());

    let index = files.binary_search_by_key(&since.as_os_str(), |file| {
        file.file_name().expect("note file path should have a file name")
    })
    .unwrap_or_else(|index| index); //error means no exact match was found

    files.drain(0..index);
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_drop_files_before_a_given_date() -> anyhow::Result<()> {
        let old_file = PathBuf::from("/some/path/2010-12-31-old-file");
        let new_file = PathBuf::from("/some/path/2020-12-31-new-file");

        let all_files = vec![old_file.clone(), new_file.clone()];

        {
            let mut files = all_files.clone();
            drop_from_sorted_files_before(since_year(1900), &mut files);
            assert_eq!(files, all_files);
        }

        {
            let mut files = all_files.clone();
            drop_from_sorted_files_before(since_year(2010), &mut files);
            assert_eq!(files, all_files);
        }

        {
            let mut files = all_files.clone();
            drop_from_sorted_files_before(since_year(2011), &mut files);
            assert_eq!(files, vec![new_file]);
        }

        {
            let mut files = all_files.clone();
            drop_from_sorted_files_before(since_year(2021), &mut files);
            assert!(files.is_empty());
        }
        Ok(())
    }

    fn since_year(year: u16) -> Since {
        Since::from(chrono::NaiveDate::from_ymd_opt(year.into(), 12, 31).unwrap())
    }
}
