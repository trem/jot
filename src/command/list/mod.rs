mod drop;
mod since;

use std::{fs, io, path::{Path, PathBuf}};

use since::Since;


/// List all files in alphabetical order.
#[derive(clap::Parser)]
pub struct ListCli {
    /// Date of the oldest file to list, e.g. "2021-12-31" or "3 months ago".
    #[arg(long)]
    since: Option<Since>,
}

impl ListCli {
    pub fn execute(self, data_dir: &Path) -> anyhow::Result<()> {
        let mut files = list_files_alphabetically(data_dir)?;

        if let Some(since_date) = self.since {
            drop::drop_from_sorted_files_before(since_date, &mut files);
        }

        for file in files {
            println!("file://{}", file.display());
        }
        Ok(())
    }
}

fn list_files_alphabetically(data_dir: &Path) -> io::Result<Vec<PathBuf>> {
    let mut files = fs::read_dir(data_dir)?
        .filter_map(|entry| {
            log::warn!("Could not read entry in data directory: {entry:?}");
            entry.ok()
        })
        .map(|entry| entry.path())
        .filter(|path| path.is_file())
        .collect::<Vec<_>>();

    files
        .sort_by(|a, b| a.file_name().cmp(&b.file_name()));

    Ok(files)
}



#[cfg(test)]
mod tests {
    use assert_fs::prelude::*;

    use super::*;

    #[test]
    fn should_list_files_in_alphabetical_order() -> anyhow::Result<()> {
        let data_dir = assert_fs::TempDir::new()?;

        let file1 = data_dir.child("1970-01-01-dazzling-camel.md");
        let file2 = data_dir.child("2000-04-04-polarising-dinosaur.md");
        let file3 = data_dir.child("2020-08-08-fancy-mosquito.md");

        //different order for creation than alphabetical to force sorting
        file2.touch()?;
        file3.touch()?;
        file1.touch()?;

        let mut result = list_files_alphabetically(&data_dir)?.into_iter();
        assert_eq!(result.next(), Some(file1.to_path_buf()));
        assert_eq!(result.next(), Some(file2.to_path_buf()));
        assert_eq!(result.next(), Some(file3.to_path_buf()));
        assert_eq!(result.next(), None);

        Ok(())
    }
}
