use std::fs::{self, File};
use std::ops::Not;
use std::path::Path;
use std::io::{self, BufReader, Read};


/// Remove empty files.
#[derive(clap::Parser)]
pub struct CleanCli {
    #[arg(long, help="Print which files would be deleted, without deleting anything.")]
    dry_run: bool,
}
impl CleanCli {
    pub fn execute(self, data_dir: &Path) -> io::Result<()> {

        let mut buffer = [0; 64];

        for entry in fs::read_dir(data_dir)? {
            let path = entry?.path();

            if path.is_file() && file_empty_or_only_whitespace(&path, &mut buffer)? {
                if self.dry_run {
                    println!("Would delete empty file: {}", path.display());
                } else {
                    fs::remove_file(&path)?;
                    println!("Deleted empty file: {}", path.display());
                }
            }
        }
        Ok(())
    }
}

fn file_empty_or_only_whitespace(path: &Path, buffer: &mut [u8]) -> io::Result<bool> {
    let mut file = BufReader::new(File::open(path)?);

    loop {
        let bytes_read = file.read(&mut buffer[..])?;
        if bytes_read == 0 { //end of file
            return Ok(true);
        }

        if buffer[..bytes_read].trim_ascii().is_empty().not() {
            return Ok(false);
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use crate::util::test;
    use assert_fs::prelude::*;

    #[test]
    fn remove_empty_files() -> test::Result {
        let temp = assert_fs::TempDir::new()?;

        temp.child("empty.txt").touch()?;
        temp.child("whitespace.txt").write_str(" \n")?;

        let file_with_content = temp.child("content.txt");
        file_with_content.write_str("Hello, world!")?;

        let cli = CleanCli {
            dry_run: true,
        };
        cli.execute(temp.path())?;

        assert_eq!(fs::read_dir(temp.path())?.count(), 3);


        let cli = CleanCli {
            dry_run: false,
        };
        cli.execute(temp.path())?;

        let mut files = fs::read_dir(temp.path())?;
        assert_eq!(files.next().unwrap()?.path(), file_with_content.to_path_buf());
        assert!(files.next().is_none()); //no more files

        Ok(())
    }
}
