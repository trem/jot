use std::io;
use std::fs::{File, DirBuilder};
use std::process::Command;
use std::path::{Path, PathBuf};

use crate::DEFAULT_FILE_EXTENSION;


/// Create a new file.
#[derive(clap::Parser)]
pub struct AddCli {
    /// File name extension to use for newly created files. Useful, if you want to write Markdown, AsciiDoc, Org Mode etc..
    #[arg(long, default_value=DEFAULT_FILE_EXTENSION)]
    pub file_extension: String,
}
impl AddCli {
    pub fn execute(self, data_dir: &Path) -> io::Result<()> {
        let file_path = create_file(data_dir, &self.file_extension)?;

        Command::new("xdg-open")
            .arg(&file_path)
            .spawn()
            .expect("Could not launch editor.")
            .wait()?;

        Ok(())
    }
}

fn create_file(data_dir: &Path, file_extension: &str) -> io::Result<PathBuf> {
    let mut file_path = data_dir.to_path_buf();

    let _ = DirBuilder::new().recursive(true).create(&file_path); //ignore errors


    file_path.push(generate_file_name());
    file_path.set_extension(file_extension);

    File::create(&file_path).unwrap_or_else(|_| panic!("Failed to create file '{}'.", &file_path.display()));
    println!("Created file: {}", file_path.display());

    Ok(file_path)
}

pub fn generate_file_name() -> String {
    let date = chrono::Local::now().date_naive();

    let adjective = fantastic_snail::random::adjective();
    let noun = fantastic_snail::random::noun();

    format!("{date}-{adjective}-{noun}")
}


#[cfg(test)]
mod tests {
    use super::*;
    use crate::util::test;
    use std::fs;

    #[test]
    fn create_randomly_named_file() -> test::Result {
        let temp = assert_fs::TempDir::new()?;

        create_file(temp.path(), "txt")?;

        assert_eq!(fs::read_dir(temp.path())?.count(), 1);
        Ok(())
    }
}
