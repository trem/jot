use std::fmt;

pub fn link(text: impl fmt::Display) -> impl fmt::Display {
    console::style(text).blue().underlined()
}

pub fn match_(text: impl fmt::Display) -> impl fmt::Display {
    console::style(text).green().bold()
}

pub fn number(text: impl fmt::Display) -> impl fmt::Display {
    console::style(text).blue()
}
