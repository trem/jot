use std::{fmt, fs, num::NonZeroUsize, ops::Not, path::{Path, PathBuf}};

use regex::Regex;

use super::{highlight, line::{Line, Match}};


pub fn file_search(file: &Path, regex: &Regex) -> FileSearchResult {
    let file_content = fs::read_to_string(file)
        .map_err(|cause| FileError {
            file: file.to_path_buf(),
            cause,
        })?;

    let lines = file_search_impl(&file_content, regex);


    Ok(FileSearch {
        file: file.to_path_buf(),
        lines,
    })
}

fn file_search_impl(file_content: &str, regex: &Regex) -> Vec<Line> {
    file_content
        .split_inclusive('\n')
        .enumerate()
        .map(|(index, line)| {
            let matches = regex.find_iter(line)
                .map(|result| Match {
                    start: result.start(),
                    end: result.end(),
                }).collect();

            Line {
                number: NonZeroUsize::new(index + 1).expect("Line number should always be larger than 0."),
                content: line.to_owned(),
                matches,
            }
        }).collect()
}


pub type FileSearchResult = std::result::Result<FileSearch, FileError>;

#[derive(Debug)]
pub struct FileSearch {
    pub file: PathBuf,
    pub lines: Vec<Line>,
}
impl FileSearch {
    pub fn render(&self, f: &mut fmt::Formatter<'_>, number_of_context_lines: usize) -> fmt::Result {
        writeln!(f, "{}", highlight::link(
            format!("file://{}", self.file.display()) //prefix with "file://" to make it clickable in (more) terminal emulators
        ))?;

        let lines = self.lines.iter()
            .filter(|line| line.content.trim().is_empty().not())
            .collect::<Vec<_>>();

        let mut show_lines = vec![false; lines.len()];


        for (index, line) in lines.iter().enumerate() {
            if line.matches.is_empty().not() {
                let context_start = index.saturating_sub(number_of_context_lines);
                let context_end = index.saturating_add(number_of_context_lines).min(lines.len() - 1);

                show_lines[context_start ..= context_end]
                    .iter_mut()
                    .for_each(|show_line| *show_line = true);
            }
        }


        let shown_lines = lines.iter().zip(show_lines)
            .filter_map(|(line, show_line)| show_line.then_some(line))
            .collect::<Vec<_>>();

        let number_width = shown_lines.last()
            .map_or(0.0, |last_line| last_line.number.get() as f64)
            .log10().floor() as usize + 1;

        let next_line_indices = shown_lines.iter()
            .map(|line| line.number)
            .skip(1)
            .chain(std::iter::once(NonZeroUsize::new(1).unwrap()));

        for (line, next_line_index) in shown_lines.iter().zip(next_line_indices) {
            let next_line_skips_ahead = next_line_index > line.number.saturating_add(1);
            line.render(f, number_width, next_line_skips_ahead)?;
        }

        Ok(())
    }
}


#[derive(Debug, thiserror::Error)]
#[error("Error while searching file '{file:?}': {cause}")]
pub struct FileError {
    pub file: PathBuf,
    pub cause: std::io::Error,
}


#[cfg(test)]
mod tests {
    use indoc::indoc;

    use super::*;

    #[test]
    fn happy_flow() -> anyhow::Result<()> {
        let file_content = indoc!("
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Justo eget magna fermentum iaculis eu.
        ");

        let regex = crate::command::search::parse_regex(r"dolor")?;

        let lines = file_search_impl(file_content, &regex);

        assert_eq!(
            lines.iter()
                .map(|line| line.content.clone())
                .collect::<String>(),
            file_content
        );

        let mut matches = lines[0].matches.iter();
        assert_eq!(
            matches.next(),
            Some(&Match { start: 12, end: 17 })
        );
        assert_eq!(
            matches.next(),
            Some(&Match { start: 103, end: 108 })
        );
        assert_eq!(
            matches.next(),
            None
        );

        let mut matches = lines[1].matches.iter();
        assert_eq!(
            matches.next(),
            None
        );

        assert!(lines.get(2).is_none());

        Ok(())
    }
}
