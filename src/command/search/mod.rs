use std::path::Path;

use anyhow::Context;
use regex::{Regex, RegexBuilder};

mod dir_search;
mod file_search;
mod highlight;
mod line;

/// Search for a string in previously created files.
#[derive(clap::Parser)]
pub struct SearchCli {
    pattern: String,
}
impl SearchCli {
    pub fn execute(self, data_dir: &Path) -> anyhow::Result<()> {
        let regex = parse_regex(&self.pattern)?;

        let number_of_context_lines = 2;
        let dir_search = dir_search::dir_search(data_dir, regex, number_of_context_lines)?;

        println!("{}", dir_search);

        Ok(())
    }
}


fn parse_regex(pattern: &str) -> anyhow::Result<Regex> {
    let regex = RegexBuilder::new(pattern)
        .case_insensitive(true)
        .build()
        .context(format!("Failed to parse pattern '{pattern}'"))?;

    Ok(regex)
}
