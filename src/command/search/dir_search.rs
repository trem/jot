use std::{fmt, fs, io, ops::Not, path::Path};

use regex::Regex;

use super::file_search;


pub fn dir_search(data_dir: &Path, regex: Regex, number_of_context_lines: usize) -> io::Result<DirSearch> {

    let file_results = fs::read_dir(data_dir)?
        .filter_map(|entry| {
            log::warn!("Could not read entry in data directory: {entry:?}");
            entry.ok()
        })
        .filter(|entry| entry.path().is_file())
        .map(move |file| file_search::file_search(&file.path(), &regex))
        .collect();

    Ok(DirSearch {
        file_results,
        number_of_context_lines,
    })
}


pub struct DirSearch {
    pub file_results: Vec<file_search::FileSearchResult>,
    pub number_of_context_lines: usize,
}
impl fmt::Display for DirSearch {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut file_searches = self.file_results.iter()
            .filter_map(|file_result| {
                file_result
                    .as_ref()
                    .inspect_err(|cause| log::warn!("{cause}"))
                    .ok()
            })
            .filter(|file_result| file_result.lines.iter().any(|result| result.matches.is_empty().not()))
            .collect::<Vec<_>>();

        file_searches.sort_unstable_by(|a, b| {
            let a_modified = a.file.metadata().unwrap().modified().unwrap();
            let b_modified = b.file.metadata().unwrap().modified().unwrap();

            a_modified.cmp(&b_modified)
        });

        for file_search in file_searches {
            file_search.render(f, self.number_of_context_lines)?;
            writeln!(f)?;
        }
        Ok(())
    }
}



#[cfg(test)]
mod tests {
    use assert_fs::prelude::*;
    use indoc::indoc;

    use super::*;

    #[test]
    fn happy_flow() -> anyhow::Result<()> {
        let temp = assert_fs::TempDir::new()?;

        temp.child("ignored_dir")
            .create_dir_all()?;

        let file1 = temp.child("file1.txt");
        let file1_content = indoc!("
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Justo eget magna fermentum iaculis eu.
        ");
        file1.write_str(file1_content)?;

        let file2 = temp.child("file2.txt");
        let file2_content = indoc!("
            Dolor purus non enim praesent elementum.
            Lacus viverra vitae congue eu.
            Ultricies mi quis hendrerit dolor magna eget.
        ");
        file2.write_str(file2_content)?;


        let regex = crate::command::search::parse_regex(r"dolor")?;
        let number_of_context_lines = 2;
        let mut result = dir_search(temp.path(), regex, number_of_context_lines)?
            .file_results
            .into_iter();


        let result1 = result.next().unwrap()?;
        let result2 = result.next().unwrap()?;
        assert!(result.next().is_none());

        assert_eq!(result1.file, file1.to_path_buf());
        assert_eq!(result2.file, file2.to_path_buf());

        Ok(())
    }
}
