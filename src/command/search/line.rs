use std::{collections::BTreeSet, fmt, num::NonZeroUsize};

use super::highlight;


#[derive(Clone, Debug)]
pub struct Line {
    pub number: NonZeroUsize,
    pub content: String,
    pub matches: BTreeSet<Match>,
}


#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Match {
    ///inclusive
    pub start: usize,
    ///exclusive
    pub end: usize,
}

impl Line {
    pub fn render(&self, f: &mut fmt::Formatter<'_>, number_width: usize, next_line_skips_ahead: bool) -> fmt::Result {
        {
            let marker = if next_line_skips_ahead {
                "﹍"
            } else {
                "  "
            };
            let number = format!("  {:number_width$}{marker}│ ", self.number);
            write!(f, "{}", highlight::number(number))?;
        }

        let matches = self.matches.clone().into_iter();

        let mut previous_match = Match {
            start: 0,
            end: 0,
        };

        for current_match in matches {
            write!(f, "{}", &self.content[previous_match.end .. current_match.start])?;

            write!(f, "{}", highlight::match_(
                &self.content[current_match.start .. current_match.end]
            ))?;

            previous_match = current_match;
        }
        write!(f, "{}", &self.content[previous_match.end..])?;

        Ok(())
    }
}
