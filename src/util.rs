#[cfg(test)]
pub mod test {
    pub type Result = std::result::Result<(), Box<dyn std::error::Error>>;
}
