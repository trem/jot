use clap::{Parser, Subcommand};

use std::path::PathBuf;
use std::env;


mod command;
mod util;

const DEFAULT_FILE_EXTENSION: &str = "md";


#[derive(Parser)]
#[command(about="Open a text editor with an empty file *somewhere* on your hard drive.", version)]
struct Cli {
    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Subcommand)]
enum Commands {
    Add(command::add::AddCli),
    Clean(command::clean::CleanCli),
    #[command(alias="ls")]
    List(command::list::ListCli),
    /// Print the directory where files are created.
    Pwd,
    Search(command::search::SearchCli),
}
impl Default for Commands {
    fn default() -> Self {
        Self::Add(command::add::AddCli {
            file_extension: DEFAULT_FILE_EXTENSION.to_string(),
        })
    }
}


fn main() -> anyhow::Result<()> {
    let program_name = "jot";
    env_logger::init();

    let opt = Cli::parse();
    let data_dir = data_directory(program_name);

    let subcommand = opt.command.unwrap_or_default();

    match subcommand {
        Commands::Add(cli) => cli.execute(&data_dir)?,
        Commands::Clean(cli) => cli.execute(&data_dir)?,
        Commands::List(cli) => cli.execute(&data_dir)?,
        Commands::Pwd => {
            println!("{}", data_dir.display());
        }
        Commands::Search(cli) => cli.execute(&data_dir)?,
    };
    Ok(())
}


fn data_directory(program_name: &str) -> PathBuf {

    #![allow(deprecated)] //env::home_dir() is deprecated due to Windows bug; we don't support Windows anyways

    let data_dir = env::var("XDG_DATA_HOME").map(PathBuf::from)
        .or(env::home_dir().map(|home| home.join(".local/share")).ok_or(()))
        .or(env::var("USER").map(|user_name|
            PathBuf::from(format!("/home/{}/.local/share/", user_name))
        ))
        .expect("Failed to find suitable directory for the note files, because $XDG_DATA_HOME, $HOME and $USER were not set.");

    data_dir.join(program_name)
}




#[cfg(test)]
mod tests {
    use super::*;

    #[test] fn determine_correct_data_directory() {
        let result = data_directory("hello");

        assert!(result.starts_with("/home/"), "Returned path '{}' does not start with '/home/'.", result.display());
        assert!(result.ends_with(".local/share/hello"), "Returned path '{}' does not end with '.local/share/hello'.", result.display());
    }
}
