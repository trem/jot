# Jot

Small utility for opening an empty file in your text editor to take quick notes.

Most text editors will open an empty file when you launch them. Jot enhances that by creating the file on disk, so that:
* you can reflexively hit Ctrl+S without the save dialog asking you for a file path.
* the file *is* saved somewhere on disk, in case you do need it again.

Only Linux is supported.

